#!/usr/bin/env bb
(load-file "rebalance.clj")

(def main-distribution
  (-> "example.edn"
    slurp
    edn/read-string))

(let [current-distribution (doall (traverse-distribution main-distribution))
      total (reduce + (map :current-value current-distribution))
      new-distribution (distribute total current-distribution)]
  (doall (print-new-distribution new-distribution))
  (println "Total:" total)
  (recommend-traverse {:children new-distribution}))
