(require '[clojure.string :as string])

;(def api-url "https://api-invest.tinkoff.ru/openapi/")
(def api-url "https://api-invest.tinkoff.ru/openapi/sandbox/")

(def token
  (format "Bearer %s"
    (-> "key.txt"
      slurp
      string/split-lines
      first)))

(curl/post (str api-url "sandbox/register")
           {:headers {"Authorization" token}})

(defn get-figi [what]
  (let [{:keys [body]} (curl/get (str api-url "market/" what)
                                 {:headers {"Authorization" token}})
        data (-> body
                (json/parse-string true)
                (get-in [:payload :instruments]))]
    (zipmap (map :ticker data) (map #(select-keys % [:figi :currency]) data))))

(def FIGI (apply merge (map get-figi ["currencies" "stocks" "bonds" "etfs"])))

(defn figi->price [figi]
  (let [{:keys [body]} (curl/get (str api-url "market/orderbook")
                                 {:headers {"Authorization" token},
                                  :query-params {"figi" figi,
                                                 "depth" "1"}})]
    (-> body
      (json/parse-string true)
      (get-in [:payload :lastPrice]))))

(declare currencies)

(defn get-price
  ([item]
   (get-price item currencies))
  ([item currencies]
   (let [{:keys [figi currency]} item
         currency-cost (get currencies (keyword currency) 1)
         price (figi->price figi)]
     (* price currency-cost))))

(defn average [lst]
  (/ (reduce + lst) (count lst)))

(defn to-metals [metals-data]
  (into {}
        (for [[k jk] {:XAU :au :XAG :ag}]
          [k (->> metals-data jk (map :value) average)])))

(def metals
  (let [{:keys [body]} (curl/get "https://alfabank.ru/ext-json/0.2/exchange/metal/"
                                 {:query-params {"mode" "rest",
                                                 "limit" "1",
                                                 "offset" "0"}})]
    (-> body
      (json/parse-string true)
      to-metals)))

(def currencies
  (let [rub (/ (get-price (get FIGI "USD000UTSTOM") {}))
        currencies-base {:USD 1, :RUB rub}]
    (merge currencies-base
           {:EUR (get-price (get FIGI "EUR_RUB__TOM") currencies-base)}
           (into {} (for [[k v] metals] [k (* rub v)])))))

(defn commodity-price [commodity]
  (or (get currencies (keyword commodity))
      (get-price (or (get FIGI commodity)))))
