(require '[clojure.string :as string])

(load-file "prices.clj")
(load-file "format.clj")
(load-file "recommend.clj")
(load-file "hledger.clj")

(defn update-leaf [value node]
  (let [{:keys [quant price]} node
        unit-size (or quant 1)
        units (int (/ value (* price unit-size)))
        new-amount (* units unit-size)]
    (assoc node
           :new-value (* new-amount price)
           :new-amount new-amount)))

(declare distribute)

(defn update-branch [value node]
  (let [updated-children (distribute value (:children node))]
    (assoc node
           :children updated-children
           :new-value (reduce + (map :new-value updated-children)))))

(defn distribute [total-value nodes]
  (let [total-weight (reduce + (map :weight nodes))]
    (loop [[node & nodes-left] nodes
           weight-left total-weight
           value-left total-value
           updated-nodes []]
      (if node
        (let [this-weight (if (= weight-left 0) 0 (/ (:weight node) weight-left))
              this-value (* value-left this-weight)
              func (if (:children node) update-branch update-leaf)
              updated-node (func this-value node)]
          (recur nodes-left
                 (- weight-left (:weight node))
                 (- value-left (:new-value updated-node))
                 (conj updated-nodes updated-node)))
        updated-nodes))))

(defn handle-leaf-node [node]
  (let [{:keys [commodity holder account amount]} node
        commodity (or commodity (:name node))
        account (or account (format "assets:.*%s.*" (:name node)))
        query-args [account (commodity->query commodity)]
        current-amount (or amount (hledger-get-current-account-balance query-args))
        price (commodity-price commodity)
        current-value (* current-amount price)]
    (assoc node
           :price price
           :current-amount current-amount
           :current-value current-value
           :display-name (or (:orig-name node) commodity account))))

(declare traverse-distribution)

(defn handle-branch-node [node]
  (let [{:keys [children name]} node
        children (traverse-distribution children node)
        current-value (reduce + (map :current-value children))]
    (assoc node
           :children children
           :current-value current-value)))

(defn collect-commodities [nodes]
  (set
   (reduce
    (fn [result node]
      (let [{:keys [commodity name children]} node]
        (if children
          (concat result (collect-commodities children))
          (conj result (or commodity name)))))
    [] nodes)))

(defn traverse-distribution [nodes & [data]]
  (for [node nodes]
    (let [merged (if (nil? data)
                   node
                   (merge (dissoc data :children)
                          node
                          {:orig-name (:name node)}))]
      ((if (:children node)
         handle-branch-node
         handle-leaf-node)
       merged))))

(defn print-new-distribution [nodes & [spaces]]
  (for [node nodes]
    (let [{:keys [name current-value children display-name new-value]} node
          spaces (or spaces 0)]
      (if spaces
        (print (apply str (repeat spaces " "))))
      (println (or display-name name) (with-precision 2 current-value) "->" (with-precision 2 new-value))
      (if children
        (doall (print-new-distribution children (inc spaces)))))))

(defn preload-prices [distribution]
  (if (.exists (io/file "prices.edn"))
    (let [prices (-> "prices.edn"
                     slurp
                     edn/read-string)]
      (def currencies (:currencies prices))
      (def FIGI (:commodities prices))))
  (doall (map commodity-price (collect-commodities distribution)))
  (spit "prices.edn" {:currencies currencies :commodities FIGI}))
