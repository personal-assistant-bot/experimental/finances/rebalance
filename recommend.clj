(load-file "format.clj")
(load-file "prices.clj")
(load-file "hledger.clj")

(defn recommend-buy [holder change what result cost]
  (if (> change (/ (- result change) 10))
    (print "+" holder ": Buy ")
    (print "?" holder ": Maybe buy "))
  (if (= change result)
    (println change what "for" (format-amount cost))
    (println change "more" what "up to" result "for" (format-amount cost))))

(defn recommend-sell [holder change what result cost]
  (if (> change (/ (+ result change) 10))
    (print "-" holder ": Sell ")
    (print "?" holder ": Maybe sell "))
  (if (= 0 result)
    (println "all" change what "for" (format-amount cost))
    (println change what "down to" result "for" (format-amount cost))))

(defn recommend-traverse [node]
  (if (:children node)
    (dorun (map recommend-traverse (:children node)))
    (let [{:keys [quant commodity holder]} node
          result (:new-amount node)
          what (:display-name node)
          change (- result (:current-amount node))
          cost (- (:new-value node) (:current-value node))]
      (cond
        (< 0 change) (recommend-buy holder change what result cost)
        (= 0 change) (println " " holder ": Keep" result what)
        (> 0 change) (recommend-sell holder (- change) what result (- cost))))))
