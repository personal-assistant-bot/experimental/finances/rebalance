(require '[clojure.string :as string])

(def hledger-command '("hledger" "-f" "./example.journal"))
(def hledger-bal '("bal" "--value=end,$" "--format" "%(total)"))
(def hledger-commodity '("bal" "--flat" "--format" "%(total)"))

(defn get-total-for [accts]
  (let [cmd (concat hledger-command hledger-bal accts)
        {:keys [out]} (apply shell/sh cmd)]
    (-> out
        string/split-lines
        last
        (subs 1)
        edn/read-string
        (* 100)
        int)))

(defn get-current [what holder]
  (let [commodity (if (= what 'USD) "\\$" what)
        cmd (concat hledger-command hledger-commodity
                    [(string/lower-case (str "^assets:.*:" holder))
                     (str "cur:" commodity)])
        {:keys [out]} (apply shell/sh cmd)
        result (-> out
                   string/split-lines
                   last)]
    (edn/read-string
      (if (= \$ (first result))
          (subs result 1)
        (let [parts (string/split result #" ")]
          (if (= "EUR" (first parts))
              (second parts)
            (first parts)))))))

(defn hledger-get-current-account-balance [args]
  (let [cmd (concat hledger-command hledger-commodity args)
        {:keys [out]} (apply shell/sh cmd)
        result (-> out
                   string/split-lines
                   last)]
    (edn/read-string
      (if (= \$ (first result))
          (subs result 1)
        (let [parts (string/split result #" ")]
          (if (= "EUR" (first parts))
              (second parts)
            (first parts)))))))

(defn commodity->query [commodity]
  (str "cur:"
       (cond
         (= commodity 'USD) "\\$"
         (= commodity 'RUB) ""
         :else commodity)))
